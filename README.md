# 小程序开发助手

**修改自minapp-vscode扩展**


微信小程序标签、属性的智能补全（同时支持原生小程序、mpvue 和 wepy 框架，并提供 snippets）

#### @we-app 可以和扩展完美搭配
> 该扩展目的就是为了更好的支持@we-app（@we-app是自用小程序框架，大家可以尝试）
```bash
# 安装脚手架
npm i -g @we-app/cli

# 初始化项目
min init demo
```

有需要帮助的请点：[这里](https://gitee.com/genqing/minapp-vscode/issues)

## 增加的功能
|功能|描述|
|---|---|
|**json跳转**|支持json跳转自定义组件|
|**less**|支持less样式智能提示|
|**支持公共组件**|支持app.json中`usingComponents`引入的组件|
|**wxml跳转组件**|支持wxml中tag跳转到自定义的组件|
|**组件可选参数**| 自定义组件prop如果是可选列表，可通过 @enum注释指定可选参数|
|**方法提示**| 组件bind方法提示（不通用，只支持typescript类写法）|
|**组件属性烤串化**| 组件的属性在wxml中提示为 `a-b-c` 格式|


## 使用帮助
#### 1、自定组件可选参数
> 注意：@enum 注释不能换行
```ts
properties: {
  /** 
   * 用于 form 组件 submit | reset 
   * @enum [{"value": "submit", "desc": "提交表单"}, {"value": "reset", "desc": "重置表单"}]
   */
  formType: String,
}
```
### 2、样式自动完成配置
强烈建议设置全局样式，可以提供完整样式的提示
```json
// 全局样式从根目录开始
{
  "minapp-vscode.globalStyleFiles": ["src/app.less"],
}
```