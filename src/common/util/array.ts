/**
 * 数组元素去重
 * @param src  
 * @param key  按key来去重
 */
 export function unique<T>(src: T[], key?: string): T[] {
  let rest: T[] = [];

  if (!Array.isArray(src)) {
    return src;
  }

  let tmp: any[] = [] 
  src.forEach(item => {
    if (key) {
      // @ts-ignore
      if (!tmp.includes(item[key])) {
        rest.push(item);
        // @ts-ignore
        tmp.push(item[key]);
      }
    } else {
      if (!rest.includes(item)) {
        rest.push(item);
      }
    }
  })

  return rest;
}