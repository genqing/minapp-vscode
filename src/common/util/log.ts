import { window } from 'vscode';

const output = window.createOutputChannel("@minapp")

export const logger = {
  log: (msg: string) => {
    output.appendLine(msg);
  }
}