/**
 * 转为驼峰格式字符串
 */
export function camelize(str: string) {
  return str.replace(/[-_](\w)/g, (_r, k: string) => k.toUpperCase());
}

/**
 * 转为烤串格式字符串
 * @param str 
 */
export function kebablize(str: string) {
  return str.replace(/[A-Z]/g, s => '-' + s.toLowerCase());
}


const REMOVE_COMMENT_REGEXP = /("([^\\\"]*(\\.)?)*")|('([^\\\']*(\\.)?)*')|(\/{2,}.*?(\r|\n))|(\/\*(\n|.)*?\*\/)/g

/**
 * 去除js、ts脚本注释
 * @param content 
 * @returns 
 */
export function removeComment(content: string) {
  return content.replace(REMOVE_COMMENT_REGEXP, word => {
    return /^\/{2,}/.test(word) || /^\/\*/.test(word) ? "" : word;
  })
}